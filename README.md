# Boness Generator

[Yeoman](http://yeoman.io) generator that scaffolds a Skinn web project.

## Getting Started

### 1. Setting up Vagrant

* Create a new project folder.
* Add details of your new project to your Homestead.yaml.

```
sites:
    - map: example.skinn.local
      to: /home/vagrant/Code/example.skinn.local/public

databases:
    - example
```

* Create a new entry in your /etc/hosts file.

```
192.168.10.10     example.skinn.local
```

* Reload your vagrant: `vagrant reload --provision`.

### 2. Running the generator

* SSH into your virtual machine: `vagrant ssh` and navigate to your project folder.
* Install both Yeoman and this generator (if you haven't already): `npm i -g yo generator-boness`.
* Run the generator: `yo boness`.

### 3. Prepare your database

* Find and open the `.env` file in the root folder of your project.
* Enter your database configuration. Save and close.
* Run migrations: `artisan migrate`.
* Apply seeding: `artisan db:seed`

## License

[MIT license](http://opensource.org/licenses/MIT)
