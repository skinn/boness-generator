# Boness Generator

**Warning!** This is an alternative way of installing a new Skinn project. You should always try to install using the [official generator](https://www.npmjs.com/package/generator-boness). Only follow this guide if all else fails.

## Getting Started

### 1. Setting up Vagrant

* Create a new project folder.
* Add details of your new project to your Homestead.yaml.
```
sites:
    - map: example.skinn.local
      to: /home/vagrant/Code/example.skinn.local/public

databases:
    - example
```
* Create a new entry in your /etc/hosts file.
```
192.168.10.10     example.skinn.local
```
* Reload your vagrant: `vagrant reload --provision`.

### 2. Create a new project

* Open your terminal. SSH into your virtual machine: `vagrant ssh` and navigate to your project folder.
* Create a new Laravel project: `composer create-project --prefer-dist laravel/laravel . "5.2.*"`
* Add the following snippet into your composer.json:
```
"require": {
    "php": ">=5.5.9",
    "laravel/framework": "5.2.*",
    "intervention/image": "2.3.7",
    "nesbot/carbon": "^1.21",
    "flyfish/core": "dev-master",
    "skinn/boness": "dev-master",
    "league/glide-laravel": "^1.0"
},
"autoload": {
    "classmap": [
        "database"
    ],
    "psr-4": {
        "App\\": "app/"
    },
    "files": ["app/mix.php"]
},
"repositories":
[
    {
        "type": "vcs",
        "url": "git@bitbucket.org:skinn/cms-core.git"
    },
    {
        "type": "vcs",
        "url": "git@bitbucket.org:skinn/boness.git"
    }
]
```
* Create a new file in the **app** folder and name it **mix.php**:
```
<?php

function mix($path, $manifest = false, $shouldHotReload = false)
{
    if (! $manifest) static $manifest;
    if (! $shouldHotReload) static $shouldHotReload;

    if (! $manifest) {
        $manifestPath = public_path('mix-manifest.json');
        $shouldHotReload = file_exists(public_path('hot'));

        if (! file_exists($manifestPath)) {
            throw new Exception(
                'The Laravel Mix manifest file does not exist. ' .
                'Please run "npm run webpack" and try again.'
            );
        }

        $manifest = json_decode(file_get_contents($manifestPath), true);
    }

    if (! starts_with($path, '/')) $path = "/{$path}";

    if (! array_key_exists($path, $manifest)) {
        throw new Exception(
            "Unknown Laravel Mix file path: {$path}. Please check your requested " .
            "webpack.mix.js output path, and try again."
        );
    }

    return $shouldHotReload
        ? "http://localhost:8080{$manifest[$path]}"
        : url($manifest[$path]);
}
```
* Update Composer: `composer update`.

### 3. Configure Laravel

* Add our own provider to the `providers` list in **config/app.php**: `\Flyfish\FlyfishCoreServiceProvider::class,`
* Run artisan: `artisan`.
* Add `skinn/boness` to **config/flyfish.php**

### 4. Build CMS

* Publish our files: `artisan boness:publish`.
* Install our dependencies: `npm install`.
* Build the CMS UI: `npm run build`.
* Build assets: `npm run dev`.

### 5. Prepare your database

* Find and open the `.env` file in the root folder of your project.
* Enter your database configuration. Save and close.
* Run migrations: `artisan migrate`.
* Apply seeding: `artisan db:seed`

### 6. Start developing

* Create `config.dev.json` in the root folder and configure your local settings:
```
{
  "name": "Example",
  "url": "example.skinn.local"
}
```
* Start your local dev server: `npm run watch`.

**Important**: You can only run this command from outside of your Vagrant environment (i.e. your Mac)
