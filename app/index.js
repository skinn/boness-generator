const yeoman = require('yeoman-generator');
const chalk = require('chalk');

const {
  spawnSync: spawn,
  execSync: exec
} = require('child_process');

const mkdir = require('mkdirp');
const request = require('request');

const updateNotifier = require('update-notifier');
const pkg = require('../package.json');
const versionsURL = 'http://sandbox.skinn.site/js/generator-boness/versions.json';

let isFirstClear = false;
let currentTask = 1;

module.exports = yeoman.extend({

  _getGitConfig() {
    const authorName = exec('git config --global --get user.name', {encoding: 'utf-8'}) || '';
    const authorEmail = exec('git config --global --get user.email', {encoding: 'utf-8'}) || '';
    return {authorName, authorEmail};
  },

  _clearConsole() {
    process.stdout.write(isFirstClear ? '\x1bc' : '\x1b[2J\x1b[0f');
    isFirstClear = false;
  },

  _spawn(cmd) {

    const parts = cmd.split(' ');
    const [first, ...rest] = parts;

    spawn(first, rest, {stdio: 'inherit'});

  },

  _parseName(name) {
    return name.toLowerCase().split(' ').join('-');
  },

  _parseUrl(name) {
    return name.toLowerCase().split(' ').join('.');
  },

  _parseBranch(branch) {

    if (isNaN(branch.charAt(0)))
      return `dev-${branch}`;

    return branch;

  },

  _notify(msg) {

    console.log(chalk.magenta('\u2396'), chalk.cyan(`[${currentTask}/${this.props.tasks}]`), chalk.magenta(msg));

    currentTask++;

  },

  initializing() {

    this.conflicter.force = true;

    this.props = {

      yarn: true,

      name: 'Skinn Web Project',
      coreBranch: 'master',
      bonessBranch: 'master',
      ipBranch: 'master',

      dbHost: '127.0.0.1',
      dbName: 'homestead',
      dbUser: 'homestead',
      dbPassword: 'secret',

      tasks: 8,

      nodeVersion: process.version.split('v')[1],

    };

    // Check if Yarn is installed
    try {
      exec('yarn --version >/dev/null 2>&1', {encoding: 'utf8'});
    } catch (e) {
      this.props.yarn = false;
    }

    // Get the current active versions
    var self = this;
    var done = this.async();
    request(versionsURL, function (error, response, body) {
      if (!error && response.statusCode == 200) {
         var versions = JSON.parse(body);
         self.props.coreBranch = versions.core;
         self.props.bonessBranch = versions.boness;
         self.props.ipBranch = versions.ip;
         done();
      }
    });

  },

  prompting() {

    const {authorName, authorEmail} = this._getGitConfig();

    updateNotifier({pkg}).notify({
      defer: false
    });

    return this.prompt([
      {
        type: 'input',
        name: 'name',
        message: 'Your project name',
        default: this.props.name // Default to current folder name
      },
      {
        type: 'input',
        name: 'authorName',
        message: 'Author',
        default: authorName.trim()
      },
      {
        type: 'input',
        name: 'authorEmail',
        message: 'Author Email',
        default: authorEmail.trim()
      },
      {
        type: 'input',
        name: 'url',
        message: 'Your URL for your project (as configured in Vagrant)',
        default: this._parseUrl(this.appname) // Default to current folder name
      },
      {
        type: 'input',
        name: 'coreBranch',
        message: 'What branch do you want for "flyfish/core"?',
        default: this.props.coreBranch
      },
      {
        type: 'input',
        name: 'bonessBranch',
        message: 'What branch do you want for "skinn/boness"?',
        default: this.props.bonessBranch
      },
      {
        type: 'input',
        name: 'ipBranch',
        message: 'What branch do you want for "skinn/ip"?',
        default: this.props.ipBranch
      },
      {
        type: 'input',
        name: 'dbHost',
        message: 'Your database host?',
        default: this.props.dbHost
      },
      {
        type: 'input',
        name: 'dbName',
        message: 'Your database name?',
        default: this.props.dbName
      },
      {
        type: 'input',
        name: 'dbUser',
        message: 'Your database username?',
        default: this.props.dbUser
      },
      {
        type: 'input',
        name: 'dbPassword',
        message: 'Your database password?',
        default: this.props.dbPassword
      },
    ]).then(props => {
      props.coreBranch = this._parseBranch(props.coreBranch);
      props.bonessBranch = this._parseBranch(props.bonessBranch);
      props.ipBranch = this._parseBranch(props.ipBranch);

      this.props = Object.assign(this.props, props);
    });

  },

  default: {

    composer() {

      this._notify('Installing Laravel...');
      this._spawn('composer create-project --quiet --prefer-dist laravel/laravel . 5.2.*');

    },

    composerTemplate() {

      this._notify('Updating composer.json...');
      this.fs.copyTpl(
        this.templatePath('_composer.json'),
        this.destinationPath('composer.json'),
        {
          coreBranch: this.props.coreBranch,
          bonessBranch: this.props.bonessBranch,
          ipBranch: this.props.ipBranch
        }
      );

    }

  },

  install: {

    mix() {

      this.fs.copyTpl(
        this.templatePath('_mix.php'),
        this.destinationPath('app/mix.php')
      );

    },

    composerUpdate() {

      this._notify('Updating composer...');
      this._spawn('composer update --quiet');

    },

    laravel() {

      this._notify('Configuring Laravel...');
      this.fs.copyTpl(
        this.templatePath('_app.php'),
        this.destinationPath('config/app.php')
      );
      this.fs.copyTpl(
        this.templatePath('_flyfish.php'),
        this.destinationPath('config/flyfish.php')
      );
      this.fs.copyTpl(
        this.templatePath('_config.dev.json'),
        this.destinationPath('config.dev.json'),
        {
          name: this.props.name,
          url: this.props.url
        }
      );
      this.fs.copyTpl(
        this.templatePath('_README.md'),
        this.destinationPath('README.md'),
        {
          name: this.props.name,
          authorName: this.props.authorName,
          authorEmail: this.props.authorEmail
        }
      );

    },

    addAssetsFolder() {

      mkdir('storage/app/assets', e => {
        if (e) console.error(e);
      });

    },

    publish() {

      this._notify('Publishing CMS files...');
      this._spawn('php artisan boness:publish');

    },

    copyEnvFile() {
      this.fs.copyTpl(
        this.templatePath('_.env'),
        this.destinationPath('.env'),
        {
          dbHost: this.props.dbHost,
          dbName: this.props.dbName,
          dbUser: this.props.dbUser,
          dbPassword: this.props.dbPassword
        }
      );
    },

    setKey() {
      this._spawn('php artisan key:generate');
    },

    dependencies() {

      this._notify('Installing dependencies...');

      // Copy package.json
      this.fs.copyTpl(
        this.templatePath('_package.json'),
        this.destinationPath('package.json'),
        {
          name: this._parseName(this.props.name),
          authorName: this.props.authorName,
          authorEmail: this.props.authorEmail
        }
      );

      // Install dependencies
      if (this.props.yarn) this._spawn('yarn');
      else this._spawn('npm install');

    }

  },

  end: {

    build() {

      this._notify('Building CMS...');
      this._spawn('npm run build:all');

    },

    clear() {
      // this._clearConsole();
    },

    showMessage() {
      console.log(chalk.black.bgGreen('\u2713 Installation complete!'));
    }

  }

});
